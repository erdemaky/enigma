﻿# Host: localhost  (Version 5.7.19-log)
# Date: 2017-10-16 23:13:14
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "question"
#

DROP TABLE IF EXISTS `question`;
CREATE TABLE `question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `a` varchar(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `d` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `question_name` varchar(255) DEFAULT NULL,
  `right_answer` varchar(255) DEFAULT NULL,
  `user_answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Data for table "question"
#

INSERT INTO `question` VALUES (1,'10 Sent','1 Dolar','5 Sent','2017-10-16 22:38:09','1.5 Dolar','Bir raket ve bir top 1 dolar 10 sente satılmaktadır. raketin fiyatı topun fiyatından 1 dolar fazlaysa top kaça satılmaktadır ?',NULL,'5 Sent',NULL),(2,'1','5','10','2017-10-16 22:38:09','100','5 makinenın 5 civatayı üretmesi 5 dakika sürmektedir. 100 makine 100 civatayı kaç dakikada üretir ?',NULL,'5',NULL),(3,'24','46','47','2017-10-16 22:38:09','12','Bir göldeki yosunlar her gün 2 katı kadar büyümektedir. göl 48 gün içinde tamamen yosunla kaplanıyor. gölün yarısının yosunla kaplanması kaç gün sürer ?',NULL,'47',NULL),(4,'15','16','8','2017-10-16 22:38:09','9','Arda sınıf sıralamasında en uzun ve en kısa 8. kişi ise, ardanın sınıfı kaç kişidir ?',NULL,'15',NULL),(5,'27','33','34','2017-10-16 22:38:09','36','1... 4... 9... 16... 25... ?  Sırayı hangi sayı almalıdır? ',NULL,'36',NULL),(6,'Halası','Teyzesi','Yengesi','2017-10-16 22:38:09','Anneanesi','Mezarın başında bir kız ağlıyor. Mezarda yatanın annesi ağlayan kızın annesinin kayın validesi. Mezarda yatan, ağlayan kızın nesi olur ? ',NULL,'Halası',NULL),(7,'1.5 Saat','2 Saat','1 Saat','2017-10-16 22:38:09','Yarım Saat','Doktorunuz size kullanmanız için 3 hap veriyor ve bunları yarımşar saat arayla almanızı tavsiye ediyor. İlaçların hepsini almanız ne kadar sürer ? ',NULL,'1 Saat',NULL),(8,'Doğru','Yanlış','Ahmedin *q','2017-10-16 22:38:09','','Ahmet aynanın karşısına geçip sol kulağını sağ eliyle tutarsa, kendini aynada sol kulağını sol eliyle tutuyormuş gibi görür ?',NULL,'Doğru',NULL);

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_date` datetime DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "user"
#


#
# Structure for table "user_false_question_ids"
#

DROP TABLE IF EXISTS `user_false_question_ids`;
CREATE TABLE `user_false_question_ids` (
  `user_id` bigint(20) NOT NULL,
  `false_question_ids` bigint(20) DEFAULT NULL,
  KEY `FK7olq2mkcs64r5ykuewpajsv20` (`user_id`),
  CONSTRAINT `FK7olq2mkcs64r5ykuewpajsv20` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "user_false_question_ids"
#


#
# Structure for table "user_right_question_ids"
#

DROP TABLE IF EXISTS `user_right_question_ids`;
CREATE TABLE `user_right_question_ids` (
  `user_id` bigint(20) NOT NULL,
  `right_question_ids` bigint(20) DEFAULT NULL,
  KEY `FK1cuvhyv9ntb9g7ray5j3tjn5s` (`user_id`),
  CONSTRAINT `FK1cuvhyv9ntb9g7ray5j3tjn5s` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "user_right_question_ids"
#

