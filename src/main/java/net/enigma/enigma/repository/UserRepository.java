package net.enigma.enigma.repository;

import net.enigma.enigma.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Detay on 16.10.2017.
 */
public interface UserRepository extends CrudRepository<User, Long>{



}
