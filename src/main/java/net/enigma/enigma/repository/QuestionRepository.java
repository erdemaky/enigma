package net.enigma.enigma.repository;

import net.enigma.enigma.entity.Question;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Detay on 16.10.2017.
 */
public interface QuestionRepository extends CrudRepository<Question, Long>{

    public Question findFirstByIdEquals(Long id);

}
