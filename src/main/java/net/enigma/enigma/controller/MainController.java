package net.enigma.enigma.controller;

import net.enigma.enigma.entity.Question;
import net.enigma.enigma.entity.User;
import net.enigma.enigma.repository.QuestionRepository;
import net.enigma.enigma.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created by Detay on 16.10.2017.
 */
@RestController
public class MainController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    QuestionRepository questionRepository;

    @RequestMapping("/")
    ModelAndView home(Model model, HttpServletResponse response) {
        response.addCookie(new Cookie("count", "0"));
        response.addCookie(new Cookie("username", ""));

        ModelAndView mav = new ModelAndView("index");
        mav.addObject("version", "0.1");
        return mav;
    }

    @RequestMapping(path = "questions", method = RequestMethod.GET)
    public ModelAndView getQuestion(HttpServletResponse response) {
        response.addCookie(new Cookie("count", "0"));
        response.addCookie(new Cookie("username", ""));

        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @RequestMapping(path = "questions", method = RequestMethod.POST)
    public ModelAndView getQuestion(Model model, HttpServletRequest request, HttpServletResponse response,@RequestParam("username") String username) {
        if (username != null){
            User user = new User();
            user.setUserName(username);

            user.setCreateDate(new Date());

            user = userRepository.save(user);

            response.addCookie(new Cookie("username", user.getId().toString()));
        }

        ModelAndView modelAndView = new ModelAndView("questions");
        return modelAndView;
    }

    @RequestMapping(path = "nextquestions", method = RequestMethod.POST)
    public Question nextQuestion(Model model, HttpServletRequest request, HttpServletResponse response, @CookieValue(value = "count",required = false) String count, @CookieValue(value = "username",required = false) String userName)
    throws Exception{
        if (!StringUtils.isEmpty(userName)) {
            User user = userRepository.findOne(Long.valueOf(userName));

            if (user == null)
                return null;

            int iCount = 1;
            if (!StringUtils.isEmpty(count)) {
                if (!count.equals("0"))
                    iCount = Integer.valueOf(count);
            }

            response.addCookie(new Cookie("count", String.valueOf(iCount)));
            Question question = questionRepository.findOne(Long.valueOf(iCount));

            if (question == null){
                response.addCookie(new Cookie("count", "0"));
                response.addCookie(new Cookie("username", ""));

                int score = user.getRightQuestionIds().size() * 10;
                question = new Question();
                question.setId(Long.valueOf(999));
                question.setQuestion(String.valueOf(score));
            }

            return question;
        }

        response.addCookie(new Cookie("count", "0"));
        response.addCookie(new Cookie("username", null));
        return null;
    }

    @RequestMapping(path = "setquestion", method = RequestMethod.GET)
    public ResponseEntity<String> setQuestion(Model model, HttpServletRequest req, HttpServletResponse response, @RequestParam("answer") String questionAnswer) {
        String userName = WebUtils.getCookie(req, "username").getValue();
        String count = WebUtils.getCookie(req, "count").getValue();

        Question question = questionRepository.findOne(Long.valueOf(count));
        question.setUserAnswer(questionAnswer);

        User user = userRepository.findOne(Long.valueOf(userName));

        int iCount = question.getId().intValue();
        iCount += 1;

        response.addCookie(new Cookie("count", String.valueOf(iCount)));

        if (user != null) {
            if (question.getUserAnswer().equals(question.getRightAnswer())) {
                user.getRightQuestionIds().add(question.getId());
                userRepository.save(user);
                return ResponseEntity.ok("Doğru :)");
            } else {
                user.getFalseQuestionIds().add(question.getId());
                userRepository.save(user);
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Yanlış :(");
            }
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Aha Yamuldu");
    }

}
