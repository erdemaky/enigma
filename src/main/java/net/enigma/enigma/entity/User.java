package net.enigma.enigma.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Detay on 16.10.2017.
 */
@Entity
public class User {

    @Id
    @Column(nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String userName;

    @ElementCollection
    private List<Long> rightQuestionIds = new ArrayList<>();

    @ElementCollection
    private List<Long> falseQuestionIds = new ArrayList<>();

    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<Long> getRightQuestionIds() {
        return rightQuestionIds;
    }

    public void setRightQuestionIds(List<Long> rightQuestionIds) {
        this.rightQuestionIds = rightQuestionIds;
    }

    public List<Long> getFalseQuestionIds() {
        return falseQuestionIds;
    }

    public void setFalseQuestionIds(List<Long> falseQuestionIds) {
        this.falseQuestionIds = falseQuestionIds;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
